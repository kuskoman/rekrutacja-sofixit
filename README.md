# sofixit zadanie rekrutacyjne

1. Create helm chart for deploying a HTTP server of your choosing onto a K8S cluster

Wybrałem tutaj nginx, bo jest po prostu popularny

1.1. It should be reachable from outside of the K8S cluster

Uzylem do tego ingressa [ingress.yaml](./templates/ingress.yaml). Testowane na ingress-nginx,
lecz powinno takze dzialac na nginx-ingress.

1.2. It should be reachable by "web-sofixit" name inside the K8S cluster

[service.yaml](./templates/service.yaml)

1.3. It should have persistent storage for static content.

[pvc.yaml](./templates/pvc.yaml)

1.4. It should be configurable without re-building its Docker image

Nie do końca rozumiem dlaczego miałoby wymagac rebuildu ale w tym wypadku nginx jest konfigurowalny
przez envy z [values.yaml](./values.yaml), oraz przez [configmap.yaml](./templates/configmap.yaml)

1.5. It should have its resources limited in order not to hog the underlying machine

Resource są zdefiniowane w [values.yaml](./values.yaml)

1.6. It should have authentication enabled and configurable without re-building its Docker image

Aby maksymalnie uprościc konfigurację, uzyłem tutaj htpasswd. Jest on domyślnie wyłączony i zdefiniowany
w [auth-secret.yaml](./templates/auth-secret.yaml). Aby go włączyć należy ustawić `auth.enabled` na `true`

1.7. It should have SSL encryption enabled and configurable without re-building its Docker image

Z racji naprawdę wielu sposobów na SSL termination, zdecydowałem się na załozenie, ze aplikacja będzie
instalowana na serwerze, który ma cert-managera, który będzie zarządzał certyfikatami. [certificate.yaml](./templates/certificate.yaml)


Aplikacja jest zdefiniowana jako statefulset, lecz w treści zadania nie było dokładnie określone, w jaki sposób
pliki mają byc wrzucane do nginx, więc nie implementowałem tego + załozyłem, ze statefulset jest tutaj potrzebny.

Dodatkowo values.yaml posiada adnotacje, które pozwalają [narzędziu do generowania readme od bitnami](https://github.com/bitnami/readme-generator-for-helm)
na automatyczne generowanie readme.md, mimo, że samo readme nie jest w ten sposób generowane w tym przypadku
(chciałem sobie oszczędzic pracy, działający przykład mozecie zobaczyc na jednym z moich repo: [logstash-exporter](https://github.com/kuskoman/logstash-exporter)), a dokładniej w tym pliku [readme.md](https://github.com/kuskoman/logstash-exporter/tree/master/chart).

Samym repo od logstash-exportera chciałbym się przy okazji pochwalic, bo mimo, ze korzystają z niego takie
firmy jak cloudflare, sbierbank, czy carefour, to ma tylko 20 gwiazdek :(


---

Chart ten został zdeployowany na moim prywatnym klastrze kubernetes ale sprawdzałem tylko opcje defaultowe,
oraz defaultowe z włączonym ssl, więc prawdopodobnie nie wszystko w nim działa, aczkolwiek samo repozytorium powinno
pokazac mój sposób myślenia.
